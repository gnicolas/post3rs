import { ComponentFixture, TestBed } from '@angular/core/testing';
import { PerPageComponent } from './per-page.component';

import {FormBuilder} from "@angular/forms";

describe('PerPageComponent', () => {
  let component: PerPageComponent;
  let fixture: ComponentFixture<PerPageComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PerPageComponent ],
      providers: [
        FormBuilder
      ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(PerPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
