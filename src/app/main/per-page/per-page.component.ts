import { Component, EventEmitter, Output } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';

@Component({
  selector: 'app-per-page',
  templateUrl: './per-page.component.html',
  styleUrls: ['./per-page.component.scss']
})
export class PerPageComponent {

  @Output() public eventPerPage = new EventEmitter<number>();

  public perPageForm: FormGroup;

  public pages: any[] = [
    {value: 6, viewValue: '6'},
    {value: 12, viewValue: '12'},
    {value: 24, viewValue: '24'},
    {value: 48, viewValue: '48'}
  ];

  constructor(private formBuilder: FormBuilder) { 
    this.perPageForm = this.formBuilder.group({
      pg: 6
    });
  }

  public changePerPage(value: number) {
    this.eventPerPage.emit(value);
  }

}
