import { Component, Input } from '@angular/core';
import { ApiService } from 'src/app/service/api.service';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.scss']
})

export class SearchComponent {

  public search: any = '';
  @Input() public beers: any[] = new Array([]);

  public showData: boolean = false;
  public loading: boolean = false;

  public page: number = 1;
  public perPage: number = 6;


  constructor(private apiService: ApiService) {
  }

  public async getData(food: string) {
    try {
      this.loading = true;

      const params = {
        food: food,
        page: this.page,
        per_page: this.perPage
      }

      const res = await this.apiService.getDataByFood(params);
      console.log(res);
      this.beers = res;
    } catch (ex) {
      console.log(ex);
    } finally {
      this.loading = false;
    }
  }

  public onChangeEvent(){
    this.page = 1;

    if(this.search) {
      this.getData(this.search);
      this.showData = true;
    } else {
      this.showData = false;
    }
  }

  public async back() {
    if(this.page != 1) {
      if(this.search) {
        this.page -= 1;
        await this.getData(this.search);
        this.showData = true;
      } else {
        this.showData = false;
      }
    }
  }

  public async next() {

    if(this.search) {
      if(this.beers.length == this.perPage) {
        this.page += 1;
        await this.getData(this.search);
      }
      this.showData = true;
    } else {
      this.showData = false;
    }
  }

  public eventPerPage(event: number) {
    this.perPage = event;
    this.onChangeEvent();
  }

}
