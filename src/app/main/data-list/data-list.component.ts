import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-data-list',
  templateUrl: './data-list.component.html',
  styleUrls: ['./data-list.component.scss']
})
export class DataListComponent implements OnInit {

  @Input() public beers: any[] = new Array([]);

  constructor() { }

  ngOnInit(): void {
  }

}
