export default interface BeerModel {
    name: string;
    tagline: string;
    image_url: string;
}