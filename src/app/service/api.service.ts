import { Injectable } from '@angular/core';
import axios, {AxiosInstance} from 'axios';

@Injectable({
  providedIn: 'root'
})

export class ApiService {

  public readonly instance : AxiosInstance;

    constructor() {
      this.instance = axios.create({
        baseURL: 'https://api.punkapi.com/v2'
      });
    }

    public async getDataByFood(params: any) {
      const url = `/beers`;

      try {
        const response = await this.instance.get(url, {params});
        return response.data;
      } catch (error) {
        console.log(error);
        return error;
      }
    }
}
